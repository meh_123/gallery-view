package com.example.mehuljain.galleryview;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity{
    List<CardList> cardList;
    RecyclerView.Adapter imageHolder;
    LinearLayoutManager card_manager;
    DisplayMetrics displayMetrics = new DisplayMetrics();
    int current_card_position;
    private int GET_IMAGE_REQUEST = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private Bitmap bitmap;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    //
    @BindView(R.id.cardView) RecyclerView cardLayout;
    @BindView(R.id.buttonAddMain) ImageButton add_card_main;
    @BindView(R.id.buttonAdd) ImageButton add_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        cardList = new ArrayList<>();
        cardLayout.setHasFixedSize(true);
        final LinearLayoutManager card_manager = new LinearLayoutManager(getApplicationContext());
        card_manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        cardLayout.setLayoutManager(card_manager);
        imageHolder = new ImageHolder(getApplication(), cardList);
        imageHolder.setHasStableIds(false);
        cardLayout.setAdapter(imageHolder);

        add_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardList.add(cardList.size(),new CardList());
                imageHolder.notifyItemInserted(cardList.size()-1);
                cardLayout.scrollToPosition(cardList.size()-1);
                add_card.setVisibility(View.INVISIBLE);
                add_card_main.setVisibility(View.VISIBLE);
                Log.d("size of cards",cardList.size()+"");
            }
        });

        add_card_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardList.add(cardList.size(),new CardList());
                imageHolder.notifyItemInserted(cardList.size()-1);
                cardLayout.scrollToPosition(cardList.size()-1);
                Log.d("size of cards",cardList.size()+"");
            }
        });

        verifyStoragePermissions(this);
    }

    public class ImageHolder extends RecyclerView.Adapter<MainActivity.ImageHolder.viewHolder>{
        private Context context;
        public List<CardList> cardList;
        public ImageHolder(Context context , List<CardList> list) {
            this.context = context;
            this.cardList = list;
        }

        @Override
        public MainActivity.ImageHolder.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.view_card, parent, false);

            return new viewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MainActivity.ImageHolder.viewHolder holder, final int position) {

            if (cardList.get(holder.getAdapterPosition()).getImage() != null) {
                holder.cardImage.setVisibility(View.VISIBLE);
                holder.storage.setVisibility(View.INVISIBLE);
                holder.cardImage.setImageBitmap(cardList.get(holder.getAdapterPosition()).getImage());
            } else {
                holder.storage.setVisibility(View.VISIBLE);
                holder.cardImage.setVisibility(View.INVISIBLE);
                Log.d("reached", current_card_position + "");
                holder.storage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get image from storage
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        current_card_position = holder.getAdapterPosition();
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GET_IMAGE_REQUEST);
                    }
                });
                holder.remove_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //remove the Current Card from the list
                        try {
                            current_card_position = holder.getAdapterPosition();
                            Log.d("removed card at ", current_card_position + "");
                            cardList.remove(holder.getAdapterPosition());
                            imageHolder.notifyItemRemoved(holder.getAdapterPosition());
                            imageHolder.notifyItemRangeChanged(holder.getAdapterPosition(), cardList.size());
                            Toast.makeText(getApplication(),"Removed card at Position "+(current_card_position+1),Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return cardList.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class viewHolder extends RecyclerView.ViewHolder{
            @BindView(R.id.imageCard) ImageView cardImage;
            @BindView(R.id.storage) ImageButton storage;
            @BindView(R.id.removeCard) ImageButton remove_card;
            public viewHolder(View itemView) {
                super(itemView);
                // bind all views
                ButterKnife.bind(this,itemView);
            }
        }
    }

    @Override
    // on activity result after selecting image from gallery  set image to view
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_IMAGE_REQUEST && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap scaled=decodeUri(filePath);
                CardList object = cardList.get(current_card_position);
                object.setImage(scaled);
                imageHolder.notifyDataSetChanged();
                throw new Exception("new exception");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public Bitmap decodeUri(Uri uri) {
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor imageSource = parcelFD.getFileDescriptor();

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(imageSource, null, o);

            // the new size we want to scale to
            final int REQUIRED_SIZE = 1024;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;

            int scale = 1;
            while (true) {
                if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(imageSource, null, o2);
            return bitmap;
        } catch (FileNotFoundException e) {
            // handle errors
        } catch (IOException e) {
            // handle errors
        } finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {
                    // ignored
                }
        }
        return null;
    }
    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
            }
        }
    }
    // getter
    public List<CardList> getCardList() {
        return cardList;
    }
    // setter
    public void setCardList(List<CardList> cardList) {
        this.cardList = cardList;
    }
}

