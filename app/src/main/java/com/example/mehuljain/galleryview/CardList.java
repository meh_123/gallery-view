package com.example.mehuljain.galleryview;

/**
 * Created by mehul jain on 06-05-2017.
 */

import android.graphics.Bitmap;

public class CardList implements java.io.Serializable  {
    Bitmap image;
    // getter
    public Bitmap getImage() {
        return image;
    }
    // setter
    public void setImage(Bitmap image) {
        this.image = image;
    }
}